// function to prompt user for personal information
function getUserInfo() {
  const fullName = prompt("Please enter your full name:");
  const age = prompt("Please enter your age:");
  const location = prompt("Please enter your location:");
  
  alert(`Thank you, ${fullName}! Your information has been saved.`);
  
  console.log(`Name: ${fullName}`);
  console.log(`Age: ${age}`);
  console.log(`Location: ${location}`);
}

// function to display favorite bands
function displayFavBands() {
  const bands = [
    "Toto",
    "The Beatles",
    "Bon jovi",
    "Queen",
    "Police"
  ];
  
  console.log("My top 5 favorite bands are:");
  for (let i = 0; i < bands.length; i++) {
    console.log(`${i + 1}. ${bands[i]}`);
  }
}

// function to display favorite movies
function displayFavMovies() {
  const movies = [
    { title: "Alikabok sa ilalim ng Dagat", rating: "90%" },
    { title: "Click", rating: "98%" },
    { title: "The Godfather: Part II", rating: "96%" },
    { title: "The Dark Knight", rating: "94%" },
    { title: "12 Angry Men", rating: "96%" }
  ];
  
  console.log("My top 5 favorite movies are:");
  for (let i = 0; i < movies.length; i++) {
    console.log(`${i + 1}. "${movies[i].title}" - Rotten Tomatoes rating: ${movies[i].rating}`);
  }
}

// invoke functions to display information
getUserInfo();
displayFavBands();
displayFavMovies();